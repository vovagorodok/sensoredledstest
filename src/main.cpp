#include <Logger.h>
#include <ArrayUtils.h>
#include <ArduinoPin.h>
#include <Arduino.h>
#include <iomanip>

#define DEMUX_PINS_NUM 3
#define A_DEMUX 5
#define B_DEMUX 12
#define C_DEMUX 2
#define X_DEMUX 15

#define BOARD_SQUERS_NUM 3
#define ANODE_DEMUX_PINS A_DEMUX, B_DEMUX, C_DEMUX
#define ANODE_PINS 25, 26, 27//, 4, 5, 6, 7, 8
#define CATODE_PINS 4, 32, 33//, 33, 25, 17, 16, 4
#define SENSOR_PINS 25, 26, 27//, 35, 26, 27, 14, 13

inline DigitalPin createDigitalPin(uint8_t pin)
{
  return {pin, OUTPUT, HIGH};
}
inline AnalogInputPin createAnalogPin(uint8_t pin)
{
  return {pin};
}

// DigitalOutputPin demuxEn{X_DEMUX, LOW}; 
// auto anodesDemux = makeArray<DigitalPin, DEMUX_PINS_NUM>(createDigitalPin, ANODE_DEMUX_PINS);
auto anodes = makeArray<DigitalPin, BOARD_SQUERS_NUM>(createDigitalPin, ANODE_PINS);
auto catodes = makeArray<DigitalPin, BOARD_SQUERS_NUM>(createDigitalPin, CATODE_PINS);
auto sensors = makeArray<AnalogInputPin, BOARD_SQUERS_NUM>(createAnalogPin, SENSOR_PINS);

// DigitalPin digitalAnode = createDigitalPin(25);
// DigitalPin digitalCatode1 = createDigitalPin(26);
// DigitalPin digitalCatode2 = createDigitalPin(27);
// AnalogInputPin analogAnode = createAnalogPin(25);

void setup()
{
  Serial.begin(115200);
  while (not Serial);
}

void loop()
{
  // MEASUREMENT TWO CATODES
  // digitalAnode.setMode(OUTPUT);
  // digitalAnode.on();
  // digitalCatode1.setMode(OPEN_DRAIN);
  // digitalCatode2.setMode(OUTPUT);
  // digitalCatode2.on();
  // delay(100);
  // digitalAnode.setMode(ANALOG);

  // LOG_INFO << "1. " << std::setw(4) << analogAnode.value();
  // delay(10);
  // LOG_INFO << "2. " << std::setw(4) << analogAnode.value();
  // delay(100);
  // LOG_INFO << "3. " << std::setw(4) << analogAnode.value();
  // delay(100);
  // LOG_INFO << "4. " << std::setw(4) << analogAnode.value();
  // delay(100);


  // MEASUREMENT
  for (unsigned i = 0; i < BOARD_SQUERS_NUM; i++)
  {
    catodes[i].setMode(OUTPUT_OPEN_DRAIN);
    anodes[i].setMode(OUTPUT);
    anodes[i].off();
  }
  delay(100);
  for (unsigned x = 0; x < BOARD_SQUERS_NUM; x++)
  {
    catodes[x].setMode(OUTPUT);
    catodes[x].on();
    for (unsigned y = 0; y < BOARD_SQUERS_NUM; y++)
    {
      anodes[y].setMode(OUTPUT);
      anodes[y].off();
    }
    delay(5);
    for (unsigned y = 0; y < BOARD_SQUERS_NUM; y++)
    {
      anodes[y].setMode(ANALOG);
    }
    delay(20);
    LOG_BEGIN_INFO;
    for (unsigned y = 0; y < BOARD_SQUERS_NUM; y++)
    {
      LOG_ADD_INFO << std::setw(6) << sensors[y];
    }
    LOG_END_INFO;
    catodes[x].setMode(OUTPUT_OPEN_DRAIN);
  }
  LOG_INFO;

  // INDICATION WITHOUT DEMUX
  for (unsigned i = 0; i < BOARD_SQUERS_NUM; i++)
  {
    anodes[i].setMode(OUTPUT);
    catodes[i].setMode(OUTPUT);
    catodes[i].on();
    anodes[i].off();
  }
  for (unsigned x = 0; x < BOARD_SQUERS_NUM; x++)
  {
    ScopedOff off(catodes[x]);
    for (unsigned y = 0; y < BOARD_SQUERS_NUM; y++)
    {
      ScopedOn on(anodes[y]);
      delay(100);
    }
  }

  // INDICATION WITH DEMUX
  // for (unsigned y = 0; y < BOARD_SQUERS_NUM; y++)
  // {
  //   for (unsigned demux = 0; demux < DEMUX_PINS_NUM; demux++)
  //     anodesDemux[demux] = y & (1 << demux);
  //   ScopedOn on(demuxEn);
  //   for (unsigned x = 0; x < BOARD_SQUERS_NUM; x++)
  //   {
  //     catodes[x].off(); // All LEDs on
  //   }
  //   delay(5);
  // }
}
